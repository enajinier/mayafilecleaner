import os

try:
    from PySide.QtCore import *
    from PySide.QtGui import *
    from PySide.QtUiTools import QUiLoader
except ImportError:
    from PySide2.QtCore import *
    from PySide2.QtGui import *
    from PySide2.QtWidgets import *
    from PySide2.QtUiTools import QUiLoader


class BaseWindow(QMainWindow):
    def __init__(self):
        super(BaseWindow, self).__init__()
        self.ui_file_name = "main_window"  # change this to main ui file name
        self.main_window = self.get_window(self.ui_file_name)  # type: QMainWindow

    @staticmethod
    def load_ui_widget(ui_file_path):
        ui_file = QFile(ui_file_path)
        ui_file.open(QFile.ReadOnly)
        loader = QUiLoader()
        ui = loader.load(ui_file)
        ui_file.close()
        return ui

    def get_window(self, ui_name):
        ui_file_path = self.get_ui_file_path(ui_name)
        window = self.load_ui_widget(ui_file_path)
        return window

    @staticmethod
    def get_ui_file_path(file_name):
        this_file_path = os.path.dirname(__file__)
        root_path = os.path.dirname(os.path.dirname(this_file_path))
        ui_file_name = file_name + ".ui"
        ui_file_path = os.path.join(root_path, "resources", "ui", ui_file_name)
        return ui_file_path

    # -------------------------------------------------------------------------------------------------------------------

    @staticmethod
    def create_message_box(text, icon=None):
        message_box = QMessageBox()  # type: QMessageBox
        message_box.setWindowTitle("Information")
        message_box.setText(str(text))
        if icon == "Information":
            message_box.setIcon(message_box.Information)
        elif icon == "Question":
            message_box.setIcon(message_box.Question)
        elif icon == "Warning":
            message_box.setIcon(message_box.Warning)
        elif icon == "Critical":
            message_box.setIcon(message_box.Critical)
        else:
            message_box.setIcon(message_box.NoIcon)
        message_box.exec_()
