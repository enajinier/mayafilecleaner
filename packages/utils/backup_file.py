import os
import shutil


def backup_file(ori_file_path=None, backup_path=None):
    print "Copying..."
    if not os.path.isdir(backup_path):
        os.makedirs(backup_path)
    file_name = os.path.basename(ori_file_path)
    file_name_wo_ext, file_ext = os.path.splitext(file_name)
    backup_file_path = os.path.join(backup_path, file_name)
    idx = 2
    while os.path.isfile(backup_file_path):
        file_name = file_name_wo_ext + "(" + str(idx) + ")" + file_ext
        backup_file_path = os.path.join(backup_path, file_name)
        idx = idx + 1
    else:
        pass
    shutil.copy2(ori_file_path, backup_file_path)
    print "Copy done, backup path is:", backup_file_path
    return backup_file_path
