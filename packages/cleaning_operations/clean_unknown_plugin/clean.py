from maya import cmds


def execute(maya_file_path):
    with open(maya_file_path, "r+") as maya_file:
        line_list = maya_file.readlines()
        maya_file.seek(0)
        for line in line_list:
            if line.startswith('requires "'):
                plugin = line.split(" ")[1].replace('"', '')
                if not cmds.pluginInfo(plugin, q=True, registered=True):
                    line = ""
                    print "Delete Unknown Plugin:", plugin
            maya_file.write(line)
        maya_file.truncate()
