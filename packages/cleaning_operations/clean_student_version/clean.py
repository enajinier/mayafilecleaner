import re


def execute(maya_file_path):
    file_path = maya_file_path.replace("\\", "/")
    pattern = r"fileInfo \"license\" \"student\";\s+"
    #
    with open(file_path, "r+") as maya_file:
        line = maya_file.read()
        match_list = re.findall(pattern=pattern, string=line)
        # clean student version and write file
        if len(match_list) == 1:
            new_line = line.replace(match_list[0], "")
            maya_file.seek(0)
            maya_file.write(new_line)
            maya_file.truncate()
            print "Delete line student version: Done."
