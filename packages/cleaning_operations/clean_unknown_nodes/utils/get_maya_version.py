import re


def execute(maya_file_path):
    pattern = r"(?:requires maya|fileInfo (?:\"product\" \"Maya|\"version\")) \D*(\d{4})\";"
    regex = re.compile(pattern)
    with open(maya_file_path, "r") as maya_file:
        line = maya_file.read()
    match_list = regex.findall(line)
    if len(match_list) == 3 and match_list[0] == match_list[1] and match_list[1] == match_list[2]:
        maya_version = match_list[0]
        return maya_version
