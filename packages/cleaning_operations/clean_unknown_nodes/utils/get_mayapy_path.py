import os


def execute(maya_version):
    autodesk_root_path = r"C:\Program Files\Autodesk\Maya"
    bin_maya_exe_path = r"\bin\mayapy.exe"
    maya_2016_path = autodesk_root_path + "2016" + bin_maya_exe_path
    maya_2018_path = autodesk_root_path + "2018" + bin_maya_exe_path
    maya_2019_path = autodesk_root_path + "2019" + bin_maya_exe_path
    maya_2020_path = autodesk_root_path + "2020" + bin_maya_exe_path

    if maya_version == "2016" and os.path.exists(maya_2016_path):
        return maya_2016_path
    elif maya_version == "2018" and os.path.exists(maya_2018_path):
        return maya_2018_path
    elif maya_version == "2019" and os.path.exists(maya_2019_path):
        return maya_2019_path
    elif maya_version == "2020" and os.path.exists(maya_2020_path):
        return maya_2020_path
