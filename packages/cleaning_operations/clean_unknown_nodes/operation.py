import sys
from maya import standalone

standalone.initialize(name="python")

from maya import cmds

file_path = sys.argv[1]


def cleaning(maya_file_path):
    try:
        cmds.file(maya_file_path, o=True, f=True)
        unknown_node_list = cmds.ls(type="unknown")
        for unknown_node in unknown_node_list:
            if cmds.objExists(unknown_node):
                cmds.lockNode(unknown_node, lock=False)
                cmds.delete(unknown_node)

        cmds.file(save=True, force=True, type="mayaAscii")

        sys.stdout.write(str(unknown_node_list))
        return str(unknown_node_list)

    except Exception, e:
        sys.stderr.write(str(e))
        sys.exit(-1)


cleaning(maya_file_path=file_path)
