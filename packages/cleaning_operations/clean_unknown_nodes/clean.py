import os
import re
import subprocess

from utils import get_maya_version, get_mayapy_path


def execute(file_path):
    maya_version = get_maya_version.execute(file_path)
    maya_path = get_mayapy_path.execute(maya_version)
    script_path = os.path.join(os.path.dirname(__file__), "operation.py")
    print maya_version, maya_path, script_path
    #
    if maya_path:
        command = '"{0}" "{1}" "{2}"'.format(maya_path, script_path, file_path)
        maya = subprocess.Popen(command, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = maya.communicate()
        exitcode = maya.returncode
        #
        if str(exitcode) != "0":
            print "err", err
            print "clean_unknown_nodes process finished with exitcode", exitcode
            print "error opening file: {0}".format(file_path)
        else:
            print "clean_unknown_nodes process finished with exitcode", exitcode
            try:
                print "out type: ", type(out)
            except:
                pass
            try:
                if isinstance(out, str):
                    pattern = r"u'(\S+)',?"
                    regex = re.compile(pattern)
                    unknown_nodes_list = regex.findall(out)
                    for unknown_node in unknown_nodes_list:
                        print "Delete Unknown Node:", unknown_node
                else:
                    print "Delete Unknown Nodes \n {0} <== Delete this Unknown Nodes from {1}".format(out, file_path)
            except:
                print "Delete Unknown Nodes \n {0} <== Delete this Unknown Nodes from {1}".format(out, file_path)
