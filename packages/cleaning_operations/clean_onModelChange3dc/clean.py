import re


def execute(maya_file_path):
    file_path = maya_file_path.replace("\\", "/")
    pattern = r'(createNode script -n "uiConfigurationScriptNode";(?:(?:\n.+)+onModelChange3dc)+(?:.+\n)+.+setAttr ".st" 3;\n)[^\t]'
    regex = re.compile(pattern)
    # get onModelChange3dc lines
    with open(file_path, "r+") as maya_file:
        line = maya_file.read()
        match_list = regex.findall(line)
        # clean onModelChange3dc and write file
        if len(match_list) == 1:
            new_line = line.replace(match_list[0], "")
            maya_file.seek(0)
            maya_file.write(new_line)
            maya_file.truncate()
            print "Delete line onModelChange3dc: Done."
