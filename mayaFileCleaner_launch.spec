# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['mayaFileCleaner_launch.py'],
             pathex=['.'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
a.datas += [('./resources/ui/main_window.ui', './resources/ui/main_window.ui', 'DATA')]
a.datas += [('./packages/cleaning_operations/clean_unknown_nodes/operation.py', './packages/cleaning_operations/clean_unknown_nodes/operation.py', 'DATA')]
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='mayaFileCleaner',
          debug=True,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=True,
		  version= './resources/version.rc')
