# README #

### What is this repository for? ###

* For cleaning student version (license), onModelChange3dc, unknown plugin, and unknown nodes from a maya file.
* Final tools is mayaFileCleaner.exe

### How do I get set up? ###

* Coding language
    * Python 2.7
* Additional python packages
    * PySide
    * PySide2
    * maya
* How to run tests
    * Run ./mayaFileCleaner_launch.py
* How to compile
    * Make sure pyinstaller already installed in Python
    * Change version in:
        * ./resources/version.rc
        * ./resources/ui/main_window.ui
    * Run bat_mayaFileCleaner_compile.bat
* Deployment instructions
    * Create a folder, name it:
        * _mayaFileCleaner
    * Copy all folders and files from this repository into folder _mayaFileCleaner, except:
        * .git/
        * .idea/
        * venv/
        * .gitignore
    * Zip folder _mayaFileCleaner, name it:
        * _mayaFileCleaner.zip
    * Copy these files to X:/TECH
        * runmayaFileCleaner.bat
        * _mayaFileCleaner.zip
    * Users can run this tools by running:
        * X:/TECH/runmayaFileCleaner.bat

### Who do I talk to? ###

* Repo owner or admin