import os
import shutil

try:
    from PySide.QtGui import *
    from PySide.QtCore import *
except ImportError:
    from PySide2.QtGui import *
    from PySide2.QtCore import *
    from PySide2.QtWidgets import *

from packages.ui_operations import base_window
from packages.utils import backup_file
from packages.cleaning_operations.clean_onModelChange3dc import clean as cl_onmodelchange3dc
from packages.cleaning_operations.clean_student_version import clean as cl_student_version
from packages.cleaning_operations.clean_unknown_plugin import clean as cl_unknown_plugin
from packages.cleaning_operations.clean_unknown_nodes import clean as cl_unknown_nodes


class cleanMayaFilesMainClass(base_window.BaseWindow):
    def __init__(self):
        super(cleanMayaFilesMainClass, self).__init__()
        #
        self.temp_path = r"C:\temp_mayaFileCleaner"
        #
        self.maya_file_path = ""
        self.maya_filter_name = "*.ma"
        #
        self.status_ready = "Ready."
        #
        self.statusbar = self.main_window.statusbar  # type: QStatusBar
        #
        self.maya_input_LED = self.main_window.maya_input_LED  # type: QLineEdit
        self.maya_file_dialog_BTN = self.main_window.maya_file_dialog_BTN  # type: QPushButton
        self.clean_BTN = self.main_window.clean_BTN  # type: QPushButton
        #
        self.maya_file_dialog_BTN.clicked.connect(self.open_maya_file_dialog)
        self.clean_BTN.clicked.connect(self.clean_maya_file)
        #
        self.statusbar.showMessage(self.status_ready)
        #
        self.main_window.show()

    # ------------------------------------------------------------------------------------------------------------------

    def open_maya_file_dialog(self):
        self.maya_file_path, self.maya_filter_name = QFileDialog.getOpenFileName(self, "Open Maya File",
                                                                                 filter=self.maya_filter_name,
                                                                                 selectedFilter=self.maya_filter_name)
        self.maya_input_LED.setText(self.maya_file_path)

    def clean_maya_file(self):
        print "============================================================================================"
        print "Clean Maya Files operation: Get data from form..."
        maya_file_path = self.maya_input_LED.text()
        if '"' in maya_file_path:
            maya_file_path = maya_file_path.replace('"', '')
        file_exists = os.path.exists(maya_file_path)
        #
        if maya_file_path != "" and file_exists:
            # backup file
            maya_file_name = os.path.basename(maya_file_path)
            temp_file_path = os.path.join(self.temp_path, maya_file_name)
            maya_temp_file_path = backup_file.backup_file(maya_file_path, temp_file_path)
            self.maya_file_path = maya_temp_file_path
            #
            print "Clean Maya Files operation: Cleaning file..."
            print "--------------------------------------------------------------------------------"
            print "maya_file_path:", self.maya_file_path
            print "--------------------------------------------------------------------------------"
            #
            self.statusbar.showMessage("Please wait...")
            #
            cleaning_status = ["Clean Maya Files operation: Cleaning {0}",
                               "Cleaning {0}. Please wait...",
                               "Clean Maya Files operation: Cleaning {0} ==> DONE\n\n"]

            # clean student version
            student_version = "student_version"
            print cleaning_status[0].format(student_version)
            self.statusbar.showMessage(cleaning_status[1].format(student_version))
            cl_student_version.execute(self.maya_file_path)
            print cleaning_status[2].format(student_version)

            # clean onModelChange3dc
            onModelChange3dc = "onModelChange3dc"
            print cleaning_status[0].format(onModelChange3dc)
            self.statusbar.showMessage(cleaning_status[1].format(onModelChange3dc))
            cl_onmodelchange3dc.execute(self.maya_file_path)
            print cleaning_status[2].format(onModelChange3dc)

            # clean unknown plugin
            unknown_plugin = "unknown_plugin"
            print cleaning_status[0].format(unknown_plugin)
            self.statusbar.showMessage(cleaning_status[1].format(unknown_plugin))
            cl_unknown_plugin.execute(self.maya_file_path)
            print cleaning_status[2].format(unknown_plugin)

            # clean unknown nodes
            unknown_nodes = "unknown_nodes"
            print cleaning_status[0].format(unknown_nodes)
            self.statusbar.showMessage(cleaning_status[1].format(unknown_nodes))
            cl_unknown_nodes.execute(self.maya_file_path)
            print cleaning_status[2].format(unknown_nodes)

            # replace ori maya file with the clean one from temp_mayaFileCleaner
            shutil.copy2(self.maya_file_path, maya_file_path)
            #
            print "Clean Maya Files operation: ...all done."
            self.show_message("Complete.", "Clean Maya File: done.", icon="Information")
            print "============================================================================================"

        elif maya_file_path != "" and not file_exists:
            self.show_message("Failed.", "Maya File Path is not exists.", icon="Warning")
            print "============================================================================================"
        elif maya_file_path == "":
            self.show_message("Failed.", "Input Maya File is empty", icon="Warning")
            print "============================================================================================"

    # ------------------------------------------------------------------------------------------------------------------

    def show_message(self, status, text, icon):
        message = str(text)
        print message
        self.statusbar.showMessage(status)
        self.create_message_box(message, icon=icon)
        self.statusbar.showMessage("Ready.")
