import os
import sys
import re
import shutil
import subprocess
from maya import standalone
from maya import cmds

try:
    from PySide.QtGui import *
    from PySide.QtCore import *
    from PySide import QtXml
except ImportError:
    from PySide2.QtGui import *
    from PySide2.QtCore import *
    from PySide2.QtWidgets import *
    from PySide2 import QtXml

# ----------------------------------------------------------------------

application_path = "'"
if getattr(sys, 'frozen', False):
    application_path = os.path.dirname(sys.executable)
elif __file__:
    application_path = os.path.dirname(__file__)
sys.path.append(application_path)

# ----------------------------------------------------------------------
"""
def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath('.'), relative_path)"""

# ----------------------------------------------------------------------

import mayaFileCleaner_main

reload(mayaFileCleaner_main)

if __name__ == "__main__":
    app = QApplication(sys.argv)
    main_window = mayaFileCleaner_main.cleanMayaFilesMainClass()
    sys.exit(app.exec_())
